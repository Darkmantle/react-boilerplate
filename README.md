
## Usage/Examples

### FormBuilder

See example files:

- `src/components/auth/loginForm/index.tsx`
- `src/pages/login/index.tsx`

### Generator

To use the generator, install hygen as a npm global module or prefix each command with "npx"

```bash
npm i -g hygen
```
OR
```bash
npx hygen ...
```

#### Page / Component

```bash
hygen tsxGen new
```

#### Hooks

```bash
hygen hookGen new
```

#### Context

```bash
hygen contextGen new
```